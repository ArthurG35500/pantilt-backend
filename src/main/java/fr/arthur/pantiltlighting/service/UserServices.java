package fr.arthur.pantiltlighting.service;

import fr.arthur.pantiltlighting.model.main.User;

import java.security.NoSuchAlgorithmException;
import java.util.List;

public interface UserServices {
    List<User> getAll();

    User getById(Integer id);

    User createUser(User user) throws NoSuchAlgorithmException;

    User updateUser(User user) throws NoSuchAlgorithmException;

    void deleteUser(Integer id);

    User getUserByUsername(String username);
}
