package fr.arthur.pantiltlighting.service;

import fr.arthur.pantiltlighting.model.reception.ReceptionContact;

public interface ContactService {

    ReceptionContact sendMessage(ReceptionContact receptionContact);
}
