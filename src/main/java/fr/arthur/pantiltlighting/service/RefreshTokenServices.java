package fr.arthur.pantiltlighting.service;


import fr.arthur.pantiltlighting.model.main.RefreshToken;
import fr.arthur.pantiltlighting.model.main.Token;

public interface RefreshTokenServices {

    RefreshToken getById(Integer id);

    RefreshToken createRefreshToken(String refreshToken, Token token);

    void deleteRefreshToken(Integer id);

    RefreshToken getByjwtId(String jti);

    Boolean existsByjwtId(String jti);

}
