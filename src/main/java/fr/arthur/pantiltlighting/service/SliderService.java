package fr.arthur.pantiltlighting.service;

import fr.arthur.pantiltlighting.model.main.Slider;
import fr.arthur.pantiltlighting.model.reception.ReceptionSlider;

import java.util.Set;

public interface SliderService {
    Slider getById(Integer id);

    Slider createSlider(ReceptionSlider receptionSlider);

    Set<Integer> getAllId();

    boolean deleteSlider(Integer id);

}
