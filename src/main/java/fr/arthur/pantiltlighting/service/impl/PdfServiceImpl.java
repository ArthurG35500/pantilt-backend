package fr.arthur.pantiltlighting.service.impl;

import fr.arthur.pantiltlighting.exceptions.UnknownRessourceException;
import fr.arthur.pantiltlighting.model.main.DisplayPdf;
import fr.arthur.pantiltlighting.model.main.Pdf;
import fr.arthur.pantiltlighting.model.reception.ReceptionDocument;
import fr.arthur.pantiltlighting.repository.PdfRepository;
import fr.arthur.pantiltlighting.service.PdfService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.Normalizer;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class PdfServiceImpl implements PdfService {

    private final PdfRepository pdfRepository;
    private final StockageFileServiceImpl stockageFileServiceImpl;
    private final DocumentServiceImpl documentServiceImpl;

    @Value("${serveurfile.url}")
    private String baseUrlFileManager;

    @Value("${serveurfile.upload.path}")
    private String uploadPathFileManager;

    public PdfServiceImpl(PdfRepository pdfRepository, StockageFileServiceImpl stockageFileServiceImpl1, DocumentServiceImpl documentServiceImpl) {
        this.pdfRepository = pdfRepository;
        this.stockageFileServiceImpl = stockageFileServiceImpl1;
        this.documentServiceImpl = documentServiceImpl;
    }

    @Override
    public Pdf getById(Integer id) {
        return pdfRepository.findById(id).orElseThrow(() -> new UnknownRessourceException("No Pdf with this ID"));
    }

    @Override
    public List<Pdf> getAll() {
        Sort sort = Sort.by(Sort.Direction.ASC, "id");
        return pdfRepository.findAll(sort);
    }


    @Override
    public Pdf getActivePdf() {
        return pdfRepository.getPdfByActiveTrue();
    }

    @Override
    public List<DisplayPdf> getAllDisplayPdf() {
        List<Pdf> pdfs = this.getAll();
        return pdfs.stream()
                .map(pdf -> new DisplayPdf(pdf.getId(), pdf.getName(), pdf.isActive(), pdf.getCreation()))
                .collect(Collectors.toList());
    }


    @Override
    public DisplayPdf createPdf(ReceptionDocument receptionDocument) throws IOException {
        validateReceptionDocument(receptionDocument);

        MultipartFile multipartFile = receptionDocument.getData();
        String originalFileName = multipartFile.getOriginalFilename();
        String cleanedFileName = cleanFileName(originalFileName != null ? originalFileName : "default_name");
        String remotePath = "/PDF/" + cleanedFileName;
        String fullUrl = baseUrlFileManager + uploadPathFileManager + remotePath;

        Path tempFile = Files.createTempFile("upload-", ".tmp");
        try {
            multipartFile.transferTo(tempFile);
            boolean uploadSuccess = stockageFileServiceImpl.uploadFile(tempFile.toFile(), fullUrl);
            if (!uploadSuccess) {
                throw new IOException("Failed to upload file to storage service");
            }
        } catch (IOException e) {
            throw new RuntimeException("Failed to upload file", e);
        } finally {
            Files.deleteIfExists(tempFile); // Ensures the temp file is deleted whether the upload succeeds or fails
        }

        Pdf savedPdf = pdfRepository.save(new Pdf(receptionDocument.getName(), documentServiceImpl.createDocument(stockageFileServiceImpl.getShareLink(remotePath) + "/download/" + cleanedFileName, receptionDocument.getExtension(), receptionDocument.getType(), remotePath)));

        return new DisplayPdf(savedPdf.getId(), savedPdf.getName(), savedPdf.isActive(), savedPdf.getCreation());
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean deletePdf(Integer id) {

        if (!pdfRepository.existsById(id)) {
            return false;
        }

        String filepath;
        try {
            filepath = pdfRepository.getReferenceById(id).getDocument().getFilepath();
        } catch (Exception e) {
            throw new IllegalStateException("Failed to retrieve document details.", e);
        }

        try {
            pdfRepository.deleteById(id);
        } catch (Exception e) {
            throw new IllegalStateException("Failed to delete pdf from repository.", e);
        }

        try {
            documentServiceImpl.deleteDocument(pdfRepository.getReferenceById(id).getDocument().getId());
        } catch (Exception e) {
            throw new IllegalStateException("Failed to delete document.", e);
        }

        try {
            if (filepath != null && !stockageFileServiceImpl.deleteFile(filepath)) {
                throw new IllegalStateException("File deletion failed");
            }
        } catch (Exception e) {
            throw new IllegalStateException("Failed to delete file.", e);
        }

        return true;
    }


    @Override
    public List<DisplayPdf> setActivePdf(Integer id) {
        List<Pdf> allDisplayPdfs = this.getAll();
        allDisplayPdfs.forEach(pdf -> pdf.setActive(false));
        pdfRepository.saveAll(allDisplayPdfs);

        Pdf displayPdfToActivate = getById(id);
        if (displayPdfToActivate != null) {
            displayPdfToActivate.setActive(true);
            pdfRepository.save(displayPdfToActivate);
        }
        return this.getAllDisplayPdf();
    }

    private void validateReceptionDocument(ReceptionDocument receptionDocument) {
        if (receptionDocument == null || receptionDocument.getData() == null ||
                receptionDocument.getExtension() == null || receptionDocument.getType() == null ||
                receptionDocument.getName() == null) {
            throw new IllegalArgumentException("Invalid receptionDocument or its properties");
        }
    }

    private String cleanFileName(String fileName) {
        String normalized = Normalizer.normalize(fileName, Normalizer.Form.NFD);
        String accentRemoved = normalized.replaceAll("\\p{InCombiningDiacriticalMarks}+", "");
        String spaceReplaced = accentRemoved.replace(" ", "_");
        return spaceReplaced.replaceAll("[^a-zA-Z0-9_.]", "");
    }
}
