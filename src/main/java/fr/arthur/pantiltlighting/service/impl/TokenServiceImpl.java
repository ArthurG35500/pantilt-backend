package fr.arthur.pantiltlighting.service.impl;


import fr.arthur.pantiltlighting.config.jwt.JwtTokenUtil;
import fr.arthur.pantiltlighting.exceptions.UnknownRessourceException;
import fr.arthur.pantiltlighting.model.main.Token;
import fr.arthur.pantiltlighting.model.main.User;
import fr.arthur.pantiltlighting.repository.TokenRepository;
import fr.arthur.pantiltlighting.service.TokenServices;
import fr.arthur.pantiltlighting.service.UserServices;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

@Service
public class TokenServiceImpl implements TokenServices {

    private final TokenRepository tokenRepository;

    private final JwtTokenUtil jwtTokenUtil;
    private final UserServices userServices;

    public TokenServiceImpl(TokenRepository tokenRepository, @Lazy JwtTokenUtil jwtTokenUtil, UserServices userServices) {
        this.tokenRepository = tokenRepository;
        this.jwtTokenUtil = jwtTokenUtil;
        this.userServices = userServices;
    }

    @Override
    public Token getById(Integer id) {
        if (id == null) {
            throw new IllegalArgumentException("ID cannot be null");
        }
        return tokenRepository.findById(id).orElseThrow(() -> new UnknownRessourceException("No Token with this ID"));
    }

    @Override
    @Transactional
    public Token createToken(String token) {
        if (token == null || token.trim().isEmpty()) {
            throw new IllegalArgumentException("Token cannot be null or empty");
        }
        // Additional token validation logic here

        Date creationDateToken = jwtTokenUtil.getCreationDateFromToken(token);
        Date expirationDateToken = jwtTokenUtil.getExpirationDateFromToken(token);
        Integer userIdToken = jwtTokenUtil.getUserIdFromToken(token);
        String jtId = jwtTokenUtil.getJtiFromToken(token);

        User userToken = userServices.getById(userIdToken);

        Token tokenBDD = new Token();
        tokenBDD.setToken(token);
        tokenBDD.setCreation(creationDateToken);
        tokenBDD.setExpiration(expirationDateToken);
        tokenBDD.setOwner(userToken);
        tokenBDD.setJti(jtId);
        return tokenRepository.save(tokenBDD);
    }

    @Override
    @Transactional
    public void deleteToken(Integer id) {
        if (id == null) {
            throw new IllegalArgumentException("ID cannot be null");
        }
        if (!tokenRepository.existsById(id)) {
            throw new UnknownRessourceException("No Token with this ID");
        }
        tokenRepository.deleteById(id);
    }

    public boolean doesJtiExist(String jti) {
        if (jti == null || jti.trim().isEmpty()) {
            throw new IllegalArgumentException("JTI cannot be null or empty");
        }
        return tokenRepository.findByJti(jti).isPresent();
    }


}
