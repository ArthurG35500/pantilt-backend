package fr.arthur.pantiltlighting.service.impl;

import fr.arthur.pantiltlighting.model.reception.ReceptionContact;
import fr.arthur.pantiltlighting.service.ContactService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.UnsupportedEncodingException;

@Service
public class ContactServiceImpl implements ContactService {

    private final JavaMailSender mailSender;
    private final SpringTemplateEngine templateEngine;

    @Value("${spring.mail.username}")
    private String fromEmail;

    @Value("${email.recipients}")
    private String[] emailRecipients;

    @Autowired
    public ContactServiceImpl(JavaMailSender mailSender, SpringTemplateEngine templateEngine) {
        this.mailSender = mailSender;
        this.templateEngine = templateEngine;
    }

    @Override
    public ReceptionContact sendMessage(ReceptionContact receptionContact) {
        MimeMessage message = mailSender.createMimeMessage();
        try {
            MimeMessageHelper helper = new MimeMessageHelper(message, true);
            Context context = prepareContext(receptionContact);
            String htmlContent = templateEngine.process("emailTemplate", context);
            helper.setFrom(fromEmail, "Pan-Tilt Web-Contact");
            helper.setTo(emailRecipients);
            helper.setSubject("Demande de contact de " + receptionContact.getName());
            helper.setText(htmlContent, true);
            mailSender.send(message);
            return receptionContact;
        } catch (MessagingException | UnsupportedEncodingException e) {
            throw new RuntimeException("Failed to send email", e);
        }
    }

    private Context prepareContext(ReceptionContact receptionContact) {
        Context context = new Context();
        context.setVariable("name", receptionContact.getName());
        context.setVariable("email", receptionContact.getEmail());
        context.setVariable("phone", receptionContact.getPhone());
        context.setVariable("message", receptionContact.getMessage());
        context.setVariable("subject", "Demande de contact de " + receptionContact.getName());
        return context;
    }
}