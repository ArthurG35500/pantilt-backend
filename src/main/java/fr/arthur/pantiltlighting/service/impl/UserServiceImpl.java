package fr.arthur.pantiltlighting.service.impl;

import fr.arthur.pantiltlighting.exceptions.NotAllowedToCreateException;
import fr.arthur.pantiltlighting.exceptions.UnknownRessourceException;
import fr.arthur.pantiltlighting.exceptions.userExceptions.UserPasswordException;
import fr.arthur.pantiltlighting.model.main.User;
import fr.arthur.pantiltlighting.repository.UserRepository;
import fr.arthur.pantiltlighting.service.UserServices;
import org.springframework.data.domain.Sort;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserServices {

    private final UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public List<User> getAll() {
        Sort sort = Sort.by(Sort.Direction.ASC, "id");
        return userRepository.findAll(sort);
    }

    @Override
    public User getById(Integer id) {
        return userRepository.findById(id).orElseThrow(() -> new UnknownRessourceException("No Plante with this ID"));
    }

    @Override
    public User createUser(User user) {

        if (this.isNameAlreadyUse(user.getUsername())) {
            user.setUsername(user.getUsername());
        } else {
            throw new NotAllowedToCreateException("Username is already user");
        }

        if (this.isPasswordValidate(user.getPassword())) {
            String passwordEncoded = new BCryptPasswordEncoder().encode(user.getPassword());
            user.setPassword(passwordEncoded);
        } else {
            throw new UserPasswordException();
        }

        return this.userRepository.save(user);
    }

    private boolean isPasswordValidate(String password) {
        return password.length() >= 8;
    }

    private void applyUpdates(User existingUser, User updates) {
        if (updates.getUsername() != null && !updates.getUsername().isEmpty() && !updates.getUsername().equals(existingUser.getUsername())) {

            if (!this.isNameAlreadyUse(updates.getUsername())) {
                throw new NotAllowedToCreateException("Username is already used");
            }
            existingUser.setUsername(updates.getUsername());
        }

        if (updates.getPassword() != null && !updates.getPassword().isEmpty()) {
            if (this.isPasswordValidate(updates.getPassword())) {
                String passwordEncoded = new BCryptPasswordEncoder().encode(updates.getPassword());
                existingUser.setPassword(passwordEncoded);
            } else {
                throw new UserPasswordException();
            }
        }

        if (updates.getChangePassword() != null) {
            existingUser.setChangePassword(updates.getChangePassword());
        }

    }

    @Override
    public User updateUser(User user) {
        User userToUpdate = this.getById(user.getId());
        applyUpdates(userToUpdate, user);
        return this.userRepository.save(userToUpdate);
    }

    @Override
    public void deleteUser(Integer id) {
        User userToDelete = this.getById(id);
        this.userRepository.delete(userToDelete);
    }

    @Override
    public User getUserByUsername(String username) {
        return this.userRepository.findByUsername(username).orElseThrow(() -> new UnknownRessourceException("No User with this username"));
    }


    private boolean isNameAlreadyUse(String username) {
        return this.userRepository.findByUsername(username).isEmpty();
    }
}
