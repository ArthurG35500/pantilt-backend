package fr.arthur.pantiltlighting.service.impl;

import fr.arthur.pantiltlighting.exceptions.UnknownRessourceException;
import fr.arthur.pantiltlighting.model.main.Document;
import fr.arthur.pantiltlighting.repository.DocumentRepository;
import fr.arthur.pantiltlighting.service.DocumentService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotBlank;

@Service
public class DocumentServiceImpl implements DocumentService {

    private final DocumentRepository documentRepository;

    public DocumentServiceImpl(DocumentRepository documentRepository) {
        this.documentRepository = documentRepository;
    }

    @Override
    public Document getById(@NotBlank Integer id) {
        if (id == null) {
            throw new IllegalArgumentException("ID cannot be null");
        }
        return documentRepository.findById(id).orElseThrow(() -> new UnknownRessourceException("No Document with this ID"));
    }

    @Override
    @Transactional
    public Document createDocument(@NotBlank String content,
                                   @NotBlank String extension,
                                   @NotBlank String type,
                                   @NotBlank String remotePath) {
        if (content.isEmpty() || extension.isEmpty() || type.isEmpty() || remotePath.isEmpty()) {
            throw new IllegalArgumentException("None of the parameters can be empty");
        }

        return documentRepository.save(new Document(content, extension, type, remotePath));
    }

    @Override
    @Transactional
    public void deleteDocument(@NotBlank Integer id) {
        if (id == null) {
            throw new IllegalArgumentException("ID cannot be null");
        }
        if (!documentRepository.existsById(id)) {
            throw new UnknownRessourceException("No Document with this ID");
        }
        documentRepository.deleteById(id);
    }


}
