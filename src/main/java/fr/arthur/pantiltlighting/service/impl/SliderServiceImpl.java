package fr.arthur.pantiltlighting.service.impl;

import fr.arthur.pantiltlighting.exceptions.ImageNotFoundException;
import fr.arthur.pantiltlighting.model.main.Image;
import fr.arthur.pantiltlighting.model.main.Slider;
import fr.arthur.pantiltlighting.model.reception.ReceptionSlider;
import fr.arthur.pantiltlighting.repository.ImageRepository;
import fr.arthur.pantiltlighting.repository.SliderRepository;
import fr.arthur.pantiltlighting.service.SliderService;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.Set;

@Service
public class SliderServiceImpl implements SliderService {

    private final SliderRepository sliderRepository;
    private final ImageRepository imageRepository;

    public SliderServiceImpl(SliderRepository sliderRepository, ImageRepository imageRepository) {
        this.sliderRepository = sliderRepository;
        this.imageRepository = imageRepository;
    }

    /**
     * Retrieves a slider by its ID.
     *
     * @param id The ID of the slider to retrieve.
     * @return The slider with the specified ID.
     * @throws RuntimeException If the ID is null or if the slider is not found.
     */
    @Override
    public Slider getById(Integer id) {
        if (id == null) {
            throw new RuntimeException("Id is null");
        }

        return this.sliderRepository.findById(id).orElseThrow(() -> new RuntimeException("Slider not found"));
    }

    /**
     * Creates a new Slider entity based on the given ReceptionSlider object.
     *
     * @param receptionSlider The ReceptionSlider object containing the necessary information for creating the Slider.
     * @return The created Slider entity.
     * @throws ImageNotFoundException If no image is found with the specified idImage.
     */
    @Override
    public Slider createSlider(ReceptionSlider receptionSlider) {

        validateReceptionSlider(receptionSlider);

        Optional<Slider> existingSlider = sliderRepository.findByImageId(receptionSlider.getIdImage());
        if (existingSlider.isPresent()) {
            // Si un Slider existant est trouvé, le retourner sans créer de nouveau
            return existingSlider.get();
        }

        Slider slider = new Slider();

        Image image = imageRepository.findById(receptionSlider.getIdImage())
                .orElseThrow(() -> new ImageNotFoundException("No image found with id: " + receptionSlider.getIdImage()));

        slider.setImage(image);

        return sliderRepository.save(slider);
    }

    @Override
    public Set<Integer> getAllId() {
        return sliderRepository.findAllIds();
    }

    @Override
    public boolean deleteSlider(Integer id) {
        if (id == null) {
            throw new IllegalArgumentException("The id cannot be null");
        }

        if (!sliderRepository.existsById(id)) {
            return true;
        }

        try {
            sliderRepository.deleteById(id);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Validate ReceptionSlider instance.
     *
     * @param receptionSlider The ReceptionSlider instance which needs to be validated.
     * @throws IllegalArgumentException If ReceptionSlider or its idImage doesn't exist.
     */
    private void validateReceptionSlider(ReceptionSlider receptionSlider) {
        if (receptionSlider == null || receptionSlider.getIdImage() == null) {
            throw new IllegalArgumentException("ReceptionSlider is null");
        }
    }
}
