package fr.arthur.pantiltlighting.service.impl;

import fr.arthur.pantiltlighting.config.jwt.JwtTokenUtil;
import fr.arthur.pantiltlighting.exceptions.UnknownRessourceException;
import fr.arthur.pantiltlighting.model.main.RefreshToken;
import fr.arthur.pantiltlighting.model.main.Token;
import fr.arthur.pantiltlighting.model.main.User;
import fr.arthur.pantiltlighting.repository.RefreshTokenRepository;
import fr.arthur.pantiltlighting.repository.TokenRepository;
import fr.arthur.pantiltlighting.service.RefreshTokenServices;
import fr.arthur.pantiltlighting.service.UserServices;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class RefreshTokenServiceImpl implements RefreshTokenServices {

    private final RefreshTokenRepository refreshTokenRepository;
    private final TokenRepository tokenRepository;
    private final JwtTokenUtil jwtTokenUtil;
    private final UserServices userServices;

    public RefreshTokenServiceImpl(RefreshTokenRepository refreshTokenRepository, TokenRepository tokenRepository, @Lazy JwtTokenUtil jwtTokenUtil, UserServices userServices) {
        this.refreshTokenRepository = refreshTokenRepository;
        this.tokenRepository = tokenRepository;
        this.jwtTokenUtil = jwtTokenUtil;
        this.userServices = userServices;
    }

    @Override
    public RefreshToken getById(Integer id) {
        if (id == null) {
            throw new IllegalArgumentException("ID cannot be null");
        }
        return refreshTokenRepository.findById(id)
                .orElseThrow(() -> new UnknownRessourceException("No RefreshToken with this ID"));
    }

    @Override
    @Transactional
    public RefreshToken createRefreshToken(String refreshToken, Token token) {
        if (refreshToken == null || refreshToken.trim().isEmpty() || token == null) {
            throw new IllegalArgumentException("Refresh token or associated token cannot be null or empty");
        }

        Date creationDateRefreshToken = jwtTokenUtil.getCreationDateFromToken(refreshToken);
        Date expirationDateRefreshToken = jwtTokenUtil.getExpirationDateFromToken(refreshToken);
        Integer userIdRefreshToken = jwtTokenUtil.getUserIdFromToken(refreshToken);

        User userRefreshToken = userServices.getById(userIdRefreshToken);

        RefreshToken refreshTokenBDD = new RefreshToken();
        refreshTokenBDD.setRefreshToken(refreshToken);
        refreshTokenBDD.setCreation(creationDateRefreshToken);
        refreshTokenBDD.setExpiration(expirationDateRefreshToken);
        refreshTokenBDD.setOwner(userRefreshToken);
        refreshTokenBDD.setToken(token);
        refreshTokenBDD.setJti(jwtTokenUtil.getJtiFromToken(refreshToken));

        return refreshTokenRepository.save(refreshTokenBDD);
    }

    @Override
    @Transactional
    public void deleteRefreshToken(Integer id) {
        if (id == null) {
            throw new IllegalArgumentException("ID cannot be null");
        }
        if (!refreshTokenRepository.existsById(id)) {
            throw new UnknownRessourceException("No RefreshToken with this ID");
        }
        refreshTokenRepository.deleteById(id);
    }

    @Override
    public RefreshToken getByjwtId(String jti) {
        try {
            return refreshTokenRepository.getRefreshTokenByJti(jti);
        } catch (Exception e) {
            throw new UnknownRessourceException("No RefreshToken with this JTI");
        }
    }

    @Override
    public Boolean existsByjwtId(String jti) {
        if (jti == null || jti.trim().isEmpty()) {
            throw new IllegalArgumentException("JTI cannot be null or empty");
        }
        return refreshTokenRepository.findByJti(jti).isPresent();
    }

    @Transactional
    @Scheduled(cron = "0 0 0 * * 0")
    public void deleteExpiredRefreshToken() {
        List<RefreshToken> expiredRefreshTokens = refreshTokenRepository.findExpiredRefreshTokens(new Date());
        if (!expiredRefreshTokens.isEmpty()) {
            List<Token> tokensToDelete = expiredRefreshTokens.stream()
                    .map(RefreshToken::getToken)
                    .filter(Objects::nonNull)
                    .collect(Collectors.toList());

            tokenRepository.deleteAll(tokensToDelete);
            refreshTokenRepository.deleteAll(expiredRefreshTokens);
        }
    }

    public boolean doesJtiExist(String jti) {
        if (jti == null || jti.trim().isEmpty()) {
            throw new IllegalArgumentException("JTI cannot be null or empty");
        }
        return refreshTokenRepository.findByJti(jti).isPresent();
    }

}
