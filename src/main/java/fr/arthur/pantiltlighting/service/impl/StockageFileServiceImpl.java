package fr.arthur.pantiltlighting.service.impl;

import fr.arthur.pantiltlighting.service.StockageFileService;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.FileEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Parser;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

@Service
public class StockageFileServiceImpl implements StockageFileService {

    private final CloseableHttpClient httpClient;

    @Value("${serveurfile.connect.username}")
    private String usernameFileManager;

    @Value("${serveurfile.connect.password}")
    private String passwordFileManager;

    @Value("${serveurfile.url}")
    private String baseUrlFileManager;

    public StockageFileServiceImpl(CloseableHttpClient httpClient) {
        this.httpClient = httpClient;
    }


    @Override
    public Boolean uploadFile(File file, String url) throws IOException {
        HttpPut put = new HttpPut(url);
        put.setEntity(new FileEntity(file, ContentType.APPLICATION_OCTET_STREAM));

        String auth = Base64.getEncoder().encodeToString((usernameFileManager + ":" + passwordFileManager).getBytes(StandardCharsets.UTF_8));
        put.setHeader("Authorization", "Basic " + auth);

        try (CloseableHttpResponse response = httpClient.execute(put)) {
            int status = response.getStatusLine().getStatusCode();
            if (status >= 200 && status < 300) {
                return true;
            } else {
                throw new IOException("Unexpected response status: " + status);
            }
        } finally {
            put.releaseConnection();
        }
    }

    public String getShareLink(String filePath) throws IOException {
        String url = baseUrlFileManager + "/ocs/v2.php/apps/files_sharing/api/v1/shares";
        HttpPost post = new HttpPost(url);

        // Prepare headers
        String auth = Base64.getEncoder().encodeToString((usernameFileManager + ":" + passwordFileManager).getBytes(StandardCharsets.UTF_8));
        post.setHeader("Authorization", "Basic " + auth);
        post.setHeader("OCS-APIRequest", "true");
        post.setHeader("Content-Type", "application/x-www-form-urlencoded");

        // Prepare body
        String body = "path=" + filePath + "&shareType=3&permissions=1";  // shareType=3 for link, permissions=1 for read-only
        post.setEntity(new StringEntity(body));

        // Execute the request and parse the response
        String response = httpClient.execute(post, httpResponse ->
                EntityUtils.toString(httpResponse.getEntity(), StandardCharsets.UTF_8)
        );

        // Parse XML to retrieve the URL
        Document doc = Jsoup.parse(response, "", Parser.xmlParser());
        Element urlElement = doc.select("url").first();
        if (urlElement != null) {
            String sharedUrl = urlElement.text();
            return sharedUrl.replace("http://", "https://");
        } else {
            throw new IOException("URL tag not found in the response XML");
        }
    }

    public Boolean deleteFile(String filePath) throws IOException {
        String url = baseUrlFileManager + "/remote.php/dav/files/" + usernameFileManager + filePath;
        HttpDelete delete = new HttpDelete(url);

        // Set the authorization header
        String auth = Base64.getEncoder().encodeToString((usernameFileManager + ":" + passwordFileManager).getBytes(StandardCharsets.UTF_8));
        delete.setHeader("Authorization", "Basic " + auth);

        // Execute the delete request
        return httpClient.execute(delete, httpResponse -> {
            int status = httpResponse.getStatusLine().getStatusCode();
            if (status >= 200 && status < 300) {
                return true;
            } else {
                throw new IOException("Unexpected response status: " + status);
            }
        });
    }
}
