package fr.arthur.pantiltlighting.service.impl;

import fr.arthur.pantiltlighting.exceptions.ImageNotFoundException;
import fr.arthur.pantiltlighting.model.main.Galerie;
import fr.arthur.pantiltlighting.model.main.Image;
import fr.arthur.pantiltlighting.model.reception.ReceptionGalerie;
import fr.arthur.pantiltlighting.repository.GalerieRepository;
import fr.arthur.pantiltlighting.repository.ImageRepository;
import fr.arthur.pantiltlighting.service.GalerieService;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GalerieServiceImpl implements GalerieService {

    private final GalerieRepository galerieRepository;

    private final ImageRepository imageRepository;

    public GalerieServiceImpl(GalerieRepository galerieRepository, ImageRepository imageRepository) {
        this.galerieRepository = galerieRepository;
        this.imageRepository = imageRepository;
    }

    @Override
    public List<Galerie> getAll() {
        Sort sort = Sort.by(Sort.Direction.ASC, "id");
        return galerieRepository.findAll(sort);
    }

    @Override
    public Galerie getById(Integer id) {
        return galerieRepository.getReferenceById(id);
    }

    @Override
    public List<Integer> getAllId() {
        return galerieRepository.findAllIds();
    }

    @Override
    public Galerie createGalerie(ReceptionGalerie receptionGalerie) {

        if (receptionGalerie == null || receptionGalerie.getIdImage() == null) {
            throw new IllegalArgumentException("ReceptionGalerie or idImage is null");
        }

        Galerie galerie = new Galerie();

        Image image = imageRepository.findById(receptionGalerie.getIdImage())
                .orElseThrow(() -> new ImageNotFoundException("No image found with id: " + receptionGalerie.getIdImage()));
        galerie.setImage(image);

        return galerieRepository.save(galerie);
    }

    @Override
    public boolean deleteGalerie(Integer id) {
        if (id == null) {
            throw new IllegalArgumentException("The id cannot be null");
        }

        if (!galerieRepository.existsById(id)) {
            return false;
        }

        try {
            galerieRepository.deleteById(id);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

}
