package fr.arthur.pantiltlighting.service.impl;

import fr.arthur.pantiltlighting.exceptions.UnknownRessourceException;
import fr.arthur.pantiltlighting.model.main.Image;
import fr.arthur.pantiltlighting.model.reception.ReceptionDocument;
import fr.arthur.pantiltlighting.repository.ImageRepository;
import fr.arthur.pantiltlighting.service.ImageService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.Normalizer;
import java.util.List;

@Service
public class ImageServiceImpl implements ImageService {
    private final ImageRepository imageRepository;
    private final StockageFileServiceImpl stockageFileServiceImpl;
    private final DocumentServiceImpl documentServiceImpl;


    @Value("${serveurfile.url}")
    private String baseUrlFileManager;

    @Value("${serveurfile.upload.path}")
    private String uploadPathFileManager;


    public ImageServiceImpl(ImageRepository imageRepository, StockageFileServiceImpl stockageFileServiceImpl, DocumentServiceImpl documentServiceImpl1) {
        this.imageRepository = imageRepository;
        this.stockageFileServiceImpl = stockageFileServiceImpl;
        this.documentServiceImpl = documentServiceImpl1;
    }

    @Override
    public Image getById(Integer id) {
        return imageRepository.findById(id).orElseThrow(() -> new UnknownRessourceException("No Image with this ID"));
    }

    @Override
    public List<Image> getAll() {
        Sort sort = Sort.by(Sort.Direction.ASC, "id");
        return imageRepository.findAll(sort);
    }

    @Override
    @Transactional
    public Image createImage(ReceptionDocument receptionDocument) throws IOException {
        validateReceptionDocument(receptionDocument);

        MultipartFile multipartFile = receptionDocument.getData();
        String originalFileName = multipartFile.getOriginalFilename();
        String cleanedFileName = cleanFileName(originalFileName != null ? originalFileName : "default_name");
        String remotePath = "/Images/" + cleanedFileName;
        String fullUrl = baseUrlFileManager + uploadPathFileManager + remotePath;

        Path tempFile = Files.createTempFile("upload-", ".tmp");
        try {
            multipartFile.transferTo(tempFile);
            boolean uploadSuccess = stockageFileServiceImpl.uploadFile(tempFile.toFile(), fullUrl);
            if (!uploadSuccess) {
                throw new IOException("Failed to upload file to storage service");
            }
        } catch (IOException e) {
            throw new RuntimeException("Failed to upload file", e);
        } finally {
            Files.deleteIfExists(tempFile); // Ensures the temp file is deleted whether the upload succeeds or fails
        }
        return imageRepository.save(new Image(receptionDocument.getName(), documentServiceImpl.createDocument((stockageFileServiceImpl.getShareLink(remotePath) + "/preview"), receptionDocument.getExtension(), receptionDocument.getType(), remotePath)));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean deleteImage(Integer id) {
        if (!imageRepository.existsById(id)) {
            return false;
        }

        String filepath;
        try {
            filepath = imageRepository.getReferenceById(id).getDocument().getFilepath();
        } catch (Exception e) {
            throw new IllegalStateException("Failed to retrieve document details.", e);
        }

        try {
            imageRepository.deleteById(id);
        } catch (Exception e) {
            throw new IllegalStateException("Failed to delete image from repository.", e);
        }

        try {
            documentServiceImpl.deleteDocument(imageRepository.getReferenceById(id).getDocument().getId());
        } catch (Exception e) {
            throw new IllegalStateException("Failed to delete document.", e);
        }

        try {
            if (filepath != null && !stockageFileServiceImpl.deleteFile(filepath)) {
                throw new IllegalStateException("File deletion failed");
            }
        } catch (Exception e) {
            throw new IllegalStateException("Failed to delete file.", e);
        }

        return true;
    }

    private void validateReceptionDocument(ReceptionDocument receptionDocument) {
        if (receptionDocument == null || receptionDocument.getData() == null ||
                receptionDocument.getExtension() == null || receptionDocument.getType() == null ||
                receptionDocument.getName() == null) {
            throw new IllegalArgumentException("Invalid receptionDocument or its properties");
        }
    }

    private String cleanFileName(String fileName) {
        String normalized = Normalizer.normalize(fileName, Normalizer.Form.NFD);
        String accentRemoved = normalized.replaceAll("\\p{InCombiningDiacriticalMarks}+", "");
        String spaceReplaced = accentRemoved.replace(" ", "_");
        return spaceReplaced.replaceAll("[^a-zA-Z0-9_.]", "");
    }
}
