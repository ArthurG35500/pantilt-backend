package fr.arthur.pantiltlighting.service;

import java.io.File;
import java.io.IOException;

public interface StockageFileService {
    Boolean uploadFile(File file, String url) throws IOException;

    String getShareLink(String filePath) throws IOException;

    Boolean deleteFile(String filePath) throws IOException;
}
