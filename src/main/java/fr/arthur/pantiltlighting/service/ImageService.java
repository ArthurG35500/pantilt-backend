package fr.arthur.pantiltlighting.service;

import fr.arthur.pantiltlighting.model.main.Image;
import fr.arthur.pantiltlighting.model.reception.ReceptionDocument;

import java.io.IOException;
import java.util.List;

public interface ImageService {
    Image getById(Integer id);

    List<Image> getAll();

    Image createImage(ReceptionDocument receptionDocument) throws IOException;

    boolean deleteImage(Integer id);

}
