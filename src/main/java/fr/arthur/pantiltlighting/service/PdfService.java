package fr.arthur.pantiltlighting.service;

import fr.arthur.pantiltlighting.model.main.DisplayPdf;
import fr.arthur.pantiltlighting.model.main.Pdf;
import fr.arthur.pantiltlighting.model.reception.ReceptionDocument;

import java.io.IOException;
import java.util.List;

public interface PdfService {

    Pdf getById(Integer id);

    List<Pdf> getAll();

    Pdf getActivePdf();

    List<DisplayPdf> getAllDisplayPdf();

    DisplayPdf createPdf(ReceptionDocument receptionDocument) throws IOException;

    boolean deletePdf(Integer id);

    List<DisplayPdf> setActivePdf(Integer id);


}
