package fr.arthur.pantiltlighting.service;

import fr.arthur.pantiltlighting.model.main.Document;

public interface DocumentService {
    Document getById(Integer id);

    Document createDocument(String s, String extension, String type, String remotePath);

    void deleteDocument(Integer id);


}
