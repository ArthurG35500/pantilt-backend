package fr.arthur.pantiltlighting.service;

import fr.arthur.pantiltlighting.model.main.Galerie;
import fr.arthur.pantiltlighting.model.reception.ReceptionGalerie;

import java.util.List;

public interface GalerieService {

    List<Galerie> getAll();

    Galerie getById(Integer id);

    List<Integer> getAllId();

    Galerie createGalerie(ReceptionGalerie receptionGalerie);

    boolean deleteGalerie(Integer id);

}
