package fr.arthur.pantiltlighting.service;


import fr.arthur.pantiltlighting.model.main.Token;

public interface TokenServices {

    Token getById(Integer id);

    Token createToken(String token);

    void deleteToken(Integer id);

}
