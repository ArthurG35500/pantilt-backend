package fr.arthur.pantiltlighting.model.reception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.Column;

/**
 * The ReceptionDocument class represents a document received.
 * It contains information about the document's name, data, type, and extension.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ReceptionDocument {
    @Column(nullable = false)
    private String name;
    @Column(nullable = false)
    private MultipartFile data;
    @Column(nullable = false)
    private String type;
    @Column(nullable = false)
    private String extension;
}
