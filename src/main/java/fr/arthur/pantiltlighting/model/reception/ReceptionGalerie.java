package fr.arthur.pantiltlighting.model.reception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The ReceptionGalerie class represents a reception gallery entity.
 * It contains information about the ID of the image associated with the reception gallery.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ReceptionGalerie {
    private Integer idImage;
}
