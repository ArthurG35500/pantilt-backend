package fr.arthur.pantiltlighting.model.main;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

/**
 * The DisplayPdf class represents a PDF document for display purposes.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DisplayPdf {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false, updatable = false)
    private String name;

    @Column(nullable = false)
    private boolean active = false;

    @Column(nullable = false, updatable = false)
    private Date creation;
}
