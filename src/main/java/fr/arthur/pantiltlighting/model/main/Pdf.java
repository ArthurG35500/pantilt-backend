package fr.arthur.pantiltlighting.model.main;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

/**
 * The Pdf class represents a PDF entity.
 * It contains information about the PDF's ID, name, active status, creation date, and associated Document.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "pdf")
public class Pdf {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false, updatable = false)
    private String name;

    @Column(nullable = false)
    private boolean active = false;

    @Column(nullable = false, updatable = false)
    private Date creation;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "document_id", referencedColumnName = "id")
    private Document document;

    public Pdf(String name, Document document) {
        if (name == null || document == null) {
            throw new IllegalArgumentException("Name and document cannot be null");
        }
        this.name = name;
        this.document = document;
    }

    @PrePersist
    protected void onCreate() {
        creation = new Date();
    }
}
