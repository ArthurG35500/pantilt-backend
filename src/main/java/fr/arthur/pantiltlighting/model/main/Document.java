package fr.arthur.pantiltlighting.model.main;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "document")
public class Document {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false, updatable = false)
    private String url;

    @Column(nullable = false)
    private String type;

    @Column(nullable = false, updatable = false)
    private String extension;

    @Column(nullable = false, updatable = false)
    private String filepath;

    public Document(String s, String extension, String type, String remotePath) {
        this.url = s;
        this.extension = extension;
        this.type = type;
        this.filepath = remotePath;
    }
}
