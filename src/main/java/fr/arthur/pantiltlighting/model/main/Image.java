package fr.arthur.pantiltlighting.model.main;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

/**
 * The Image class represents an image entity.
 *
 * <p>It contains information about the image's ID, name, creation date, and associated document.
 *
 * <p>The class is annotated with JPA annotations to define its mapping to the database table "image".
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "image")
public class Image {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false, updatable = false)
    private String name;

    @Column(nullable = false, updatable = false)
    private Date creation;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "document_id", referencedColumnName = "id")
    private Document document;

    public Image(String name, Document document) {
        if (name == null || document == null) {
            throw new IllegalArgumentException("Name and document cannot be null");
        }
        this.name = name;
        this.document = document;
    }

    @PrePersist
    protected void onCreate() {
        creation = new Date();
    }
}
