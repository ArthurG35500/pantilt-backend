package fr.arthur.pantiltlighting.api.v1;

import fr.arthur.pantiltlighting.api.dto.reception.ReceptionContactDto;
import fr.arthur.pantiltlighting.mapper.reception.ReceptionContactMapper;
import fr.arthur.pantiltlighting.service.ContactService;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/contact")
public class ContactApi {
    private final ContactService contactService;
    private final ReceptionContactMapper receptionContactMapper;

    public ContactApi(ContactService contactService, ReceptionContactMapper receptionContactMapper) {
        this.contactService = contactService;
        this.receptionContactMapper = receptionContactMapper;
    }

    @PostMapping(
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.TEXT_PLAIN_VALUE},
            path = "/send"
    )
    public ResponseEntity<ReceptionContactDto> sendMail(@RequestBody ReceptionContactDto receptionContactDto) {
        System.out.println("Sending message");
        return ResponseEntity.ok(this.receptionContactMapper.mapToDto(this.contactService.sendMessage(this.receptionContactMapper.mapToModel(receptionContactDto))));
    }
}
