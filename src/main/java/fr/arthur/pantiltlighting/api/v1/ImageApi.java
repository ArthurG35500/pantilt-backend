package fr.arthur.pantiltlighting.api.v1;

import fr.arthur.pantiltlighting.api.dto.main.ImageDto;
import fr.arthur.pantiltlighting.api.dto.reception.ReceptionDocumentDto;
import fr.arthur.pantiltlighting.config.AuthorizationHelper;
import fr.arthur.pantiltlighting.config.MyUserDetails;
import fr.arthur.pantiltlighting.exceptions.NotAllowedToDeleteException;
import fr.arthur.pantiltlighting.exceptions.UnknownRessourceException;
import fr.arthur.pantiltlighting.mapper.main.ImageMapper;
import fr.arthur.pantiltlighting.mapper.reception.ReceptionDocumentMapper;
import fr.arthur.pantiltlighting.model.main.Image;
import fr.arthur.pantiltlighting.model.reception.ReceptionDocument;
import fr.arthur.pantiltlighting.service.ImageService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/v1/image")
public class ImageApi {
    private final ImageService imageService;
    private final ImageMapper imageMapper;
    private final ReceptionDocumentMapper receptionDocumentMapper;

    public ImageApi(ImageService imageService, ImageMapper imageMapper, ReceptionDocumentMapper receptionDocumentMapper) {
        this.imageService = imageService;
        this.imageMapper = imageMapper;
        this.receptionDocumentMapper = receptionDocumentMapper;
    }

    @GetMapping("/id/{id}")
    public ResponseEntity<ImageDto> getPdfById(@PathVariable Integer id) {
        Image image = imageService.getById(id);

        System.out.println("-----------------");
        System.out.println(image.getDocument().getUrl());
        System.out.println("-----------------");
        return ResponseEntity.ok(imageMapper.mapToDto(image));
    }

    @GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE})
    @ApiOperation(value = "Récupérer toutes les images", response = ImageDto.class, responseContainer = "List")
    @Operation(summary = "Récupérer toutes les images")
    public ResponseEntity<List<ImageDto>> getAll() {
        return ResponseEntity.ok(
                this.imageService.getAll().stream()
                        .map(this.imageMapper::mapToDto)
                        .toList()
        );
    }

    @PostMapping(
            consumes = {MediaType.MULTIPART_FORM_DATA_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.TEXT_PLAIN_VALUE}
    )
    public ResponseEntity<ImageDto> createPdf(
            @RequestParam("name") String name,
            @RequestParam("file") MultipartFile file,
            @RequestParam("type") String type,
            @RequestParam("extension") String extension) throws IOException {

        if (file.isEmpty()) {
            return ResponseEntity.badRequest().build();
        }


        ReceptionDocumentDto documentDto = new ReceptionDocumentDto(name, file, type, extension);
        ReceptionDocument receptionDocument = receptionDocumentMapper.mapToModel(documentDto);

        ImageDto createdImage = imageMapper.mapToDto(imageService.createImage(receptionDocument));

        return ResponseEntity.ok(createdImage);
    }


    @Operation(summary = "Supprimer un PDF")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @DeleteMapping(path = "/{id}")
    public ResponseEntity<Boolean> deleteImage(@PathVariable @ApiParam(value = "Identifiant du PDF", example = "1") final Integer id, @AuthenticationPrincipal MyUserDetails userDetails) {
        AuthorizationHelper.authorizeAdmin(userDetails);
        try {
            return ResponseEntity.ok(this.imageService.deleteImage(id));
        } catch (UnknownRessourceException ure) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ure.getMessage());
        } catch (NotAllowedToDeleteException ex) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, ex.getMessage());
        }
    }
}
