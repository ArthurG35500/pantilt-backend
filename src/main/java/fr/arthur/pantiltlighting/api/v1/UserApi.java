package fr.arthur.pantiltlighting.api.v1;


import fr.arthur.pantiltlighting.api.dto.main.UserDto;
import fr.arthur.pantiltlighting.config.AuthorizationHelper;
import fr.arthur.pantiltlighting.config.MyUserDetails;
import fr.arthur.pantiltlighting.config.jwt.JwtRefreshToken;
import fr.arthur.pantiltlighting.config.jwt.JwtResponse;
import fr.arthur.pantiltlighting.config.jwt.JwtTokenUtil;
import fr.arthur.pantiltlighting.config.jwt.LoginRequest;
import fr.arthur.pantiltlighting.config.jwt.UserDetails.UserDetailsServiceImpl;
import fr.arthur.pantiltlighting.exceptions.NotAllowedToCreateException;
import fr.arthur.pantiltlighting.exceptions.NotAllowedToDeleteException;
import fr.arthur.pantiltlighting.exceptions.UnknownRessourceException;
import fr.arthur.pantiltlighting.mapper.main.UserMapper;
import fr.arthur.pantiltlighting.model.main.RefreshToken;
import fr.arthur.pantiltlighting.model.main.Token;
import fr.arthur.pantiltlighting.service.RefreshTokenServices;
import fr.arthur.pantiltlighting.service.TokenServices;
import fr.arthur.pantiltlighting.service.UserServices;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.net.URI;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("/v1/users")
public class UserApi {
    private final UserServices userServices;
    private final UserMapper userMapper;
    private final JwtTokenUtil jwtTokenUtil;
    private final AuthenticationManager authenticationManager;
    private final UserDetailsServiceImpl userDetailsService;
    private final TokenServices tokenServices;
    private final RefreshTokenServices refreshTokenServices;


    @Autowired
    public UserApi(UserServices userServices, UserMapper userMapper, JwtTokenUtil jwtTokenUtil, AuthenticationManager authenticationManager, UserDetailsServiceImpl userDetailsService, TokenServices tokenServices, RefreshTokenServices refreshTokenServices) {
        this.userServices = userServices;
        this.userMapper = userMapper;
        this.jwtTokenUtil = jwtTokenUtil;
        this.authenticationManager = authenticationManager;
        this.userDetailsService = userDetailsService;
        this.tokenServices = tokenServices;
        this.refreshTokenServices = refreshTokenServices;
    }

    @GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE})
    @ApiOperation(value = "Récupérer tous les utilisateurs", response = UserDto.class, responseContainer = "List")
    @Operation(summary = "Récupérer tous les utilisateurs")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<List<UserDto>> getAll(@AuthenticationPrincipal UserDetails userDetails) {
        AuthorizationHelper.authorizeAdmin(userDetails);
        return ResponseEntity.ok(
                this.userServices.getAll().stream()
                        .map(this.userMapper::mapToDto)
                        .toList()
        );
    }


    @GetMapping(value = "/{id}", produces = {MediaType.APPLICATION_JSON_VALUE})
    @ApiOperation(value = "Récupérer un utilisateur par son identifiant", response = UserDto.class)
    @Operation(summary = "Récupérer un utilisateur par son identifiant")
    public ResponseEntity<UserDto> getById(@PathVariable @ApiParam(value = "Identifiant de l'utilisateur", example = "1") Integer id) {
        try {
            return ResponseEntity.ok(this.userMapper
                    .mapToDto(this.userServices.getById(id)));
        } catch (UnknownRessourceException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping(value = "/getMyUser", produces = {MediaType.APPLICATION_JSON_VALUE})
    @ApiOperation(value = "Récupéré l'utilisateur connecter", response = UserDto.class)
    @Operation(summary = "Récupéré l'utilisateur connecter")
    public ResponseEntity<UserDto> checkIfChangePassword(@AuthenticationPrincipal MyUserDetails userDetails) {
        try {
            return ResponseEntity.ok(this.userMapper
                    .mapToDto(this.userServices.getById(userDetails.getId())));
        } catch (UnknownRessourceException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }


    @ApiOperation(value = "Créer un utilisateur", response = UserDto.class)
    @Operation(summary = "Créer un utilisateur")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Utilisateur créé avec succès"),
            @ApiResponse(responseCode = "400", description = "Mauvaise requête, l'utilisateur n'a pas pu être créé"),
            @ApiResponse(responseCode = "500", description = "Erreur interne du serveur")
    })
    @PostMapping(
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.TEXT_PLAIN_VALUE}
    )
    public ResponseEntity<UserDto> createUser(@RequestBody final UserDto userDto) throws NoSuchAlgorithmException {
        UserDto userDtoResponse =
                this.userMapper.mapToDto(
                        this.userServices.createUser(
                                this.userMapper.mapToModel(userDto)
                        ));

        return ResponseEntity
                .created(URI.create("/v1/users/" + userDtoResponse.getId()))
                .body(userDtoResponse);
    }

    @Operation(summary = "Mettre à jour un utilisateur existant")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "L'utilisateur a été mis à jour avec succès", content = @Content(schema = @Schema(implementation = UserDto.class))),
            @ApiResponse(responseCode = "400", description = "La syntaxe de la requête est incorrecte"),
            @ApiResponse(responseCode = "404", description = "L'utilisateur à mettre à jour est introuvable"),
            @ApiResponse(responseCode = "500", description = "Une erreur serveur s'est produite")
    })
    @PutMapping(path = "/{id}", produces = {MediaType.APPLICATION_JSON_VALUE}, consumes = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<UserDto> updateUser(@RequestBody UserDto user, @PathVariable @ApiParam(value = "Identifiant de l'utilisateur", example = "1") Integer id) {
        try {
            user.setId(id);
            UserDto userDto = userMapper.mapToDto(userServices.updateUser(userMapper.mapToModel(user)));
            return ResponseEntity.ok(userDto);
        } catch (NotAllowedToCreateException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    @Operation(summary = "Supprimer un utilisateur par son identifiant")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "L'utilisateur a été supprimé avec succès"),
            @ApiResponse(responseCode = "404", description = "L'utilisateur n'a pas été trouvé"),
            @ApiResponse(responseCode = "403", description = "La suppression n'est pas autorisée")
    })
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @DeleteMapping(path = "/{id}")
    public ResponseEntity<Void> deleteArticle(@PathVariable @ApiParam(value = "Identifiant de l'utilisateur", example = "1") final Integer id, @AuthenticationPrincipal MyUserDetails userDetails) {
        if (Objects.equals(id, userDetails.getId())) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Vous ne pouvez pas supprimer votre compte");
        }
        try {
            this.userServices.deleteUser(id);
            return ResponseEntity.noContent().build();
        } catch (UnknownRessourceException ure) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ure.getMessage());
        } catch (NotAllowedToDeleteException ex) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, ex.getMessage());
        }
    }

    @Operation(summary = "Se connecter", description = "Permet de s'authentifier avec un nom d'utilisateur et un mot de passe")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Utilisateur connecté avec succès"),
            @ApiResponse(responseCode = "400", description = "Nom d'utilisateur ou mot de passe invalide"),
            @ApiResponse(responseCode = "401", description = "Utilisateur non autorisé"),
            @ApiResponse(responseCode = "500", description = "Erreur interne du serveur")
    })
    @PostMapping("/login")
    public ResponseEntity<JwtResponse> login(@RequestBody LoginRequest loginRequest) {
        try {
            validateLoginRequest(loginRequest);

            Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));
            SecurityContextHolder.getContext().setAuthentication(authentication);
            MyUserDetails userDetails = (MyUserDetails) authentication.getPrincipal();

            Token token = generateToken(userDetails);
            RefreshToken refreshToken = generateRefreshToken(userDetails, token);

            JwtResponse jwtResponse = new JwtResponse(token.getToken(), refreshToken.getRefreshToken());

            return ResponseEntity.ok(jwtResponse);
        } catch (AuthenticationException e) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();

        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    private void validateLoginRequest(LoginRequest loginRequest) {
        if (loginRequest == null) {
            throw new IllegalArgumentException("Login request cannot be null");
        }
        if (loginRequest.getUsername() == null || loginRequest.getUsername().trim().isEmpty()) {
            throw new IllegalArgumentException("Username cannot be null or empty");
        }
        if (loginRequest.getPassword() == null || loginRequest.getPassword().trim().isEmpty()) {
            throw new IllegalArgumentException("Password cannot be null or empty");
        }
    }

    @Operation(summary = "Rafraîchir le token", description = "Permet de rafraîchir le token d'un utilisateur")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Token rafraîchi avec succès"),
            @ApiResponse(responseCode = "401", description = "Token invalide"),
            @ApiResponse(responseCode = "500", description = "Erreur interne du serveur")
    })
    @PostMapping("/refresh")
    public ResponseEntity<JwtResponse> refresh(@RequestBody JwtRefreshToken jwtRefreshToken) {
        String tokenReception = jwtRefreshToken.getRefreshToken();
        MyUserDetails userDetails = userDetailsService.loadUserById(jwtTokenUtil.getUserIdFromToken(tokenReception));

        if (!jwtTokenUtil.validateToken(tokenReception, userDetails) || !refreshTokenExist(jwtTokenUtil.getJtiFromToken(tokenReception))) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }

        RefreshToken oldRefreshToken = refreshTokenServices.getByjwtId(jwtTokenUtil.getJtiFromToken(tokenReception));
        Token token = generateToken(userDetails);
        RefreshToken refreshToken = generateRefreshToken(userDetails, token);

        refreshTokenServices.deleteRefreshToken(oldRefreshToken.getId());

        return ResponseEntity.ok(new JwtResponse(token.getToken(), refreshToken.getRefreshToken()));
    }

    @Operation(summary = "Rafraîchir le token", description = "Permet de rafraîchir le token d'un utilisateur")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Token rafraîchi avec succès"),
            @ApiResponse(responseCode = "401", description = "Token invalide"),
            @ApiResponse(responseCode = "500", description = "Erreur interne du serveur")
    })
    @PostMapping("/logout")
    public ResponseEntity<Boolean> logout(@RequestBody JwtRefreshToken jwtRefreshToken) {
        String tokenReception = jwtRefreshToken.getRefreshToken();

        if (!refreshTokenExist(jwtTokenUtil.getJtiFromToken(tokenReception))) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }

        RefreshToken refreshToken = refreshTokenServices.getByjwtId(jwtTokenUtil.getJtiFromToken(tokenReception));

        tokenServices.deleteToken(refreshToken.getToken().getId());

        refreshTokenServices.deleteRefreshToken(refreshToken.getId());

        return ResponseEntity.ok(true);
    }

    @GetMapping("/authorize")
    public ResponseEntity<Boolean> authorize() {
        return ResponseEntity.ok(true);
    }


    private Token generateToken(MyUserDetails userDetails) {
        String token = jwtTokenUtil.generateToken(userDetails);
        return tokenServices.createToken(token);
    }

    private RefreshToken generateRefreshToken(MyUserDetails userDetails, Token token) {
        String refreshToken = jwtTokenUtil.generateRefreshToken(userDetails);

        return refreshTokenServices.createRefreshToken(refreshToken, token);
    }

    private boolean refreshTokenExist(String jti) {
        return refreshTokenServices.existsByjwtId(jti);
    }
}
