package fr.arthur.pantiltlighting.api.v1;

import fr.arthur.pantiltlighting.api.dto.main.DisplayPdfDto;
import fr.arthur.pantiltlighting.api.dto.main.PdfDto;
import fr.arthur.pantiltlighting.api.dto.reception.ReceptionDocumentDto;
import fr.arthur.pantiltlighting.config.AuthorizationHelper;
import fr.arthur.pantiltlighting.config.MyUserDetails;
import fr.arthur.pantiltlighting.exceptions.NotAllowedToDeleteException;
import fr.arthur.pantiltlighting.exceptions.UnknownRessourceException;
import fr.arthur.pantiltlighting.mapper.main.DisplayPdfMapper;
import fr.arthur.pantiltlighting.mapper.main.PdfMapper;
import fr.arthur.pantiltlighting.mapper.reception.ReceptionDocumentMapper;
import fr.arthur.pantiltlighting.model.main.Pdf;
import fr.arthur.pantiltlighting.model.reception.ReceptionDocument;
import fr.arthur.pantiltlighting.service.PdfService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/v1/pdf")
public class PdfApi {

    private final PdfService pdfService;
    private final PdfMapper pdfMapper;
    private final ReceptionDocumentMapper receptionDocumentMapper;
    private final DisplayPdfMapper displayPdfMapper;

    public PdfApi(PdfService pdfService, PdfMapper pdfMapper, ReceptionDocumentMapper receptionDocumentMapper, DisplayPdfMapper displayPdfMapper) {
        this.pdfService = pdfService;
        this.pdfMapper = pdfMapper;
        this.receptionDocumentMapper = receptionDocumentMapper;
        this.displayPdfMapper = displayPdfMapper;
    }

    @GetMapping("/{id}")
    public ResponseEntity<PdfDto> getPdfById(@PathVariable Integer id, @AuthenticationPrincipal UserDetails userDetails) {
        AuthorizationHelper.authorizeAdmin(userDetails);
        Pdf pdf = pdfService.getById(id);
        return ResponseEntity.ok(pdfMapper.mapToDto(pdf));
    }

    @PostMapping(
            consumes = {MediaType.MULTIPART_FORM_DATA_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.TEXT_PLAIN_VALUE}
    )
    public ResponseEntity<DisplayPdfDto> createPdf(
            @RequestParam("name") String name,
            @RequestParam("file") MultipartFile file,
            @RequestParam("type") String type,
            @RequestParam("extension") String extension) throws IOException {

        if (file.isEmpty()) {
            return ResponseEntity.badRequest().build();
        }

        System.out.println("Received file:");
        System.out.println("Name: " + file.getOriginalFilename());
        System.out.println("Size: " + file.getSize() + " bytes");
        System.out.println("Type: " + file.getContentType());

        ReceptionDocumentDto documentDto = new ReceptionDocumentDto(name, file, type, extension);

        ReceptionDocument receptionDocument = receptionDocumentMapper.mapToModel(documentDto);
        return ResponseEntity.ok(displayPdfMapper.mapToDto(pdfService.createPdf(receptionDocument)));
    }

    @GetMapping("/active")
    public ResponseEntity<PdfDto> getActivePdf() {
        try {
            return ResponseEntity.ok(this.pdfMapper.mapToDto(pdfService.getActivePdf()));
        } catch (UnknownRessourceException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE})
    @ApiOperation(value = "Récupérer tous les PDF", response = PdfDto.class, responseContainer = "List")
    @Operation(summary = "Récupérer tous les PDF")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<List<PdfDto>> getAll(@AuthenticationPrincipal UserDetails userDetails) {
        AuthorizationHelper.authorizeAdmin(userDetails);
        return ResponseEntity.ok(
                this.pdfService.getAll().stream()
                        .map(this.pdfMapper::mapToDto)
                        .toList()
        );
    }

    @GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE}, path = "/display")
    @ApiOperation(value = "Récupérer tous les PDF", response = DisplayPdfDto.class, responseContainer = "List")
    @Operation(summary = "Récupérer tous les PDF")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<List<DisplayPdfDto>> getAllDisplayPDF(@AuthenticationPrincipal UserDetails userDetails) {
        AuthorizationHelper.authorizeAdmin(userDetails);
        return ResponseEntity.ok(this.pdfService.getAllDisplayPdf().stream()
                .map(this.displayPdfMapper::mapToDto)
                .toList());
    }

    @PutMapping(path = "/activePdf/{id}", produces = {MediaType.APPLICATION_JSON_VALUE})
    @ApiOperation(value = "Modifier un PDF", response = DisplayPdfDto.class)
    @Operation(summary = "Modifier un PDF")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<List<DisplayPdfDto>> updatePdfActive(@PathVariable @ApiParam(value = "Identifiant du PDF", example = "1") Integer id, @AuthenticationPrincipal UserDetails userDetails) {
        AuthorizationHelper.authorizeAdmin(userDetails);
        return ResponseEntity.ok(this.pdfService.setActivePdf(id).stream()
                .map(this.displayPdfMapper::mapToDto)
                .toList());
    }

    @Operation(summary = "Supprimer un PDF")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @DeleteMapping(path = "/{id}")
    public ResponseEntity<Boolean> deleteArticle(@PathVariable @ApiParam(value = "Identifiant du PDF", example = "1") final Integer id, @AuthenticationPrincipal MyUserDetails userDetails) {
        AuthorizationHelper.authorizeAdmin(userDetails);
        try {
            return ResponseEntity.ok(this.pdfService.deletePdf(id));
        } catch (UnknownRessourceException ure) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ure.getMessage());
        } catch (NotAllowedToDeleteException ex) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, ex.getMessage());
        }
    }

}
