package fr.arthur.pantiltlighting.api.v1;

import fr.arthur.pantiltlighting.api.dto.main.GalerieDto;
import fr.arthur.pantiltlighting.api.dto.reception.ReceptionGalerieDto;
import fr.arthur.pantiltlighting.config.AuthorizationHelper;
import fr.arthur.pantiltlighting.config.MyUserDetails;
import fr.arthur.pantiltlighting.exceptions.NotAllowedToDeleteException;
import fr.arthur.pantiltlighting.exceptions.UnknownRessourceException;
import fr.arthur.pantiltlighting.mapper.main.GalerieMapper;
import fr.arthur.pantiltlighting.mapper.reception.ReceptionGalerieMapper;
import fr.arthur.pantiltlighting.service.GalerieService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/v1/galerie")
public class GalerieApi {

    private final GalerieService galerieService;

    private final GalerieMapper galerieMapper;

    private final ReceptionGalerieMapper receptionGalerieMapper;

    public GalerieApi(GalerieService galerieService, GalerieMapper galerieMapper, ReceptionGalerieMapper receptionGalerieMapper) {
        this.galerieService = galerieService;
        this.galerieMapper = galerieMapper;
        this.receptionGalerieMapper = receptionGalerieMapper;
    }

    @GetMapping(path = "/allId", produces = {MediaType.APPLICATION_JSON_VALUE})
    @ApiOperation(value = "Récupérer tous les identifiants des images de la galerie", response = Integer.class, responseContainer = "List")
    @Operation(summary = "Récupérer tous les identifiants des images de la galerie")
    public ResponseEntity<List<Integer>> getAllId() {
        return ResponseEntity.ok(this.galerieService.getAllId());
    }

    @GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE})
    @ApiOperation(value = "Récupérer toutes les images de la galerie", response = GalerieDto.class, responseContainer = "List")
    @Operation(summary = "Récupérer toutes les images de la galerie")
    public ResponseEntity<List<GalerieDto>> getAll() {
        return ResponseEntity.ok(
                this.galerieService.getAll().stream()
                        .map(this.galerieMapper::mapToDto)
                        .toList()
        );
    }

    @GetMapping(path = "/id/{id}", produces = {MediaType.APPLICATION_JSON_VALUE})
    @ApiOperation(value = "Récupérer une image de la galerie", response = GalerieDto.class)
    @Operation(summary = "Récupérer une image de la galerie")
    public ResponseEntity<GalerieDto> getById(@PathVariable @ApiParam(value = "Identifiant de l'image de la galerie", example = "1") final Integer id) {
        return ResponseEntity.ok(this.galerieMapper.mapToDto(this.galerieService.getById(id)));
    }

    @PostMapping(
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.TEXT_PLAIN_VALUE}
    )
    @ApiOperation(value = "Ajouter une image à la galerie", response = GalerieDto.class)
    @Operation(summary = "Ajouter une image à la galerie")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<GalerieDto> createGalerie(@RequestBody final ReceptionGalerieDto receptionGalerieDto, @AuthenticationPrincipal MyUserDetails userDetails) {
        AuthorizationHelper.authorizeAdmin(userDetails);
        return ResponseEntity.ok(this.galerieMapper.mapToDto(this.galerieService.createGalerie(this.receptionGalerieMapper.mapToModel(receptionGalerieDto))));
    }

    @Operation(summary = "Supprimer une Image de la Galerie")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @DeleteMapping(path = "/{id}")
    public ResponseEntity<Boolean> deleteItemGalerie(@PathVariable @ApiParam(value = "Identifiant de l'item de la galerie", example = "1") final Integer id, @AuthenticationPrincipal MyUserDetails userDetails) {
        AuthorizationHelper.authorizeAdmin(userDetails);
        try {
            return ResponseEntity.ok(this.galerieService.deleteGalerie(id));
        } catch (UnknownRessourceException ure) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ure.getMessage());
        } catch (NotAllowedToDeleteException ex) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, ex.getMessage());
        }
    }

}
