package fr.arthur.pantiltlighting.api.v1;

import fr.arthur.pantiltlighting.api.dto.main.SliderDto;
import fr.arthur.pantiltlighting.api.dto.reception.ReceptionSliderDto;
import fr.arthur.pantiltlighting.config.AuthorizationHelper;
import fr.arthur.pantiltlighting.config.MyUserDetails;
import fr.arthur.pantiltlighting.exceptions.NotAllowedToDeleteException;
import fr.arthur.pantiltlighting.exceptions.UnknownRessourceException;
import fr.arthur.pantiltlighting.mapper.main.SliderMapper;
import fr.arthur.pantiltlighting.mapper.reception.ReceptionSliderMapper;
import fr.arthur.pantiltlighting.service.SliderService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Set;

@RestController
@RequestMapping("/v1/slider")
public class SliderApi {
    private final SliderService sliderService;
    private final SliderMapper sliderMapper;
    private final ReceptionSliderMapper receptionSliderMapper;

    public SliderApi(SliderService sliderService, SliderMapper sliderMapper, ReceptionSliderMapper receptionSliderMapper) {
        this.sliderService = sliderService;
        this.sliderMapper = sliderMapper;
        this.receptionSliderMapper = receptionSliderMapper;
    }


    @GetMapping(path = "/allId", produces = {MediaType.APPLICATION_JSON_VALUE})
    @ApiOperation(value = "Récupérer tous les identifiants des images du Slider", response = Integer.class, responseContainer = "List")
    @Operation(summary = "Récupérer tous les identifiants des images du Slider")
    public ResponseEntity<Set<Integer>> getAllId() {
        return ResponseEntity.ok(this.sliderService.getAllId());
    }

    @PostMapping(
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.TEXT_PLAIN_VALUE}
    )
    @ApiOperation(value = "Ajouter une image au slider", response = SliderDto.class)
    @Operation(summary = "Ajouter une image au slider")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<SliderDto> createSlider(@RequestBody final ReceptionSliderDto receptionSliderDto, @AuthenticationPrincipal MyUserDetails userDetails) {
        AuthorizationHelper.authorizeAdmin(userDetails);
        return ResponseEntity.ok(sliderMapper.mapToDto(sliderService.createSlider(receptionSliderMapper.mapToModel(receptionSliderDto))));
    }

    @Operation(summary = "Supprimer une Image du Slider")
    @ApiOperation(value = "Ajouter une image au slider", response = Boolean.class)
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @DeleteMapping(path = "/{id}")
    public ResponseEntity<Boolean> deleteSlider(@PathVariable @ApiParam(value = "Identifiant de l'item du slider", example = "1") final Integer id, @AuthenticationPrincipal MyUserDetails userDetails) {
        AuthorizationHelper.authorizeAdmin(userDetails);
        try {
            return ResponseEntity.ok(sliderService.deleteSlider(id));
        } catch (UnknownRessourceException ure) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ure.getMessage());
        } catch (NotAllowedToDeleteException ex) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, ex.getMessage());
        }
    }

    @GetMapping(path = "/id/{id}", produces = {MediaType.APPLICATION_JSON_VALUE})
    @ApiOperation(value = "Récupérer une image du Slider", response = SliderDto.class)
    @Operation(summary = "Récupérer une image du Slider")
    public ResponseEntity<SliderDto> getById(@PathVariable @ApiParam(value = "Identifiant de l'image du Slider", example = "1") final Integer id) {
        return ResponseEntity.ok(this.sliderMapper.mapToDto(this.sliderService.getById(id)));
    }


}
