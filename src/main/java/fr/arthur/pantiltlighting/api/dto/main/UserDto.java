package fr.arthur.pantiltlighting.api.dto.main;


import fr.arthur.pantiltlighting.enumeration.Role;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Data
public class UserDto {
    private Integer id;
    private String username;
    private Role role;
    private String password;
    private Boolean changePassword;
}
