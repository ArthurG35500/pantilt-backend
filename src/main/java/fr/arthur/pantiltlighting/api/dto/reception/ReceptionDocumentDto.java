package fr.arthur.pantiltlighting.api.dto.reception;

import lombok.*;
import org.springframework.web.multipart.MultipartFile;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Data
public class ReceptionDocumentDto {
    private String name;
    private MultipartFile data;
    private String type;
    private String extension;
}
