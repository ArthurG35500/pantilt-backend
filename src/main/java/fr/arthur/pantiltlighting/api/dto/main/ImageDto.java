package fr.arthur.pantiltlighting.api.dto.main;

import lombok.*;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Data
public class ImageDto {
    private Integer id;
    private String name;
    private Date creation;
    private DocumentDto document;
}
