package fr.arthur.pantiltlighting.api.dto.main;

import lombok.*;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Data
public class TokenDto {
    private Integer id;
    private String jti;
    private String token;
    private Date creation;
    private Date expiration;
    private Integer ownerId;
    private Integer refreshTokenId;
}