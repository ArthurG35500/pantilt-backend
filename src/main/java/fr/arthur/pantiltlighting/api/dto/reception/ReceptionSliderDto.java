package fr.arthur.pantiltlighting.api.dto.reception;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Data
public class ReceptionSliderDto {
    private Integer idImage;
}
