package fr.arthur.pantiltlighting.api.dto.main;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Data
public class DocumentDto {
    private Integer id;
    private String url;
    private String type;
    private String extension;
    private String filepath;
}
