package fr.arthur.pantiltlighting.api.dto.main;

import lombok.*;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Data
public class SliderDto {
    private Integer id;
    private Date creation;
    private ImageDto image;
}
