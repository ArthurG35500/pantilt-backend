package fr.arthur.pantiltlighting.api.dto.main;

import lombok.*;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Data
public class DisplayPdfDto {
    private Integer id;
    private String name;
    private boolean active;
    private Date creation;
}
