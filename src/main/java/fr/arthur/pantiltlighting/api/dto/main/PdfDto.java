package fr.arthur.pantiltlighting.api.dto.main;

import lombok.*;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Data
public class PdfDto {
    private final boolean active = false;
    private Integer id;
    private String name;
    private Date creation;
    private DocumentDto document;

}
