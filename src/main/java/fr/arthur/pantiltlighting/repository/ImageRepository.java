package fr.arthur.pantiltlighting.repository;

import fr.arthur.pantiltlighting.model.main.Image;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ImageRepository extends JpaRepository<Image, Integer> {
}
