package fr.arthur.pantiltlighting.repository;

import fr.arthur.pantiltlighting.model.main.RefreshToken;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface RefreshTokenRepository extends JpaRepository<RefreshToken, Integer> {
    @Modifying
    @Query("SELECT rt FROM RefreshToken rt WHERE rt.expiration < :currentDate")
    List<RefreshToken> findExpiredRefreshTokens(@Param("currentDate") Date currentDate);

    Optional<RefreshToken> findByJti(String jti);

    RefreshToken getRefreshTokenByJti(String jti);
}
