package fr.arthur.pantiltlighting.repository;

import fr.arthur.pantiltlighting.model.main.Galerie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GalerieRepository extends JpaRepository<Galerie, Integer> {
    @Query("SELECT g.id FROM Galerie g")
    List<Integer> findAllIds();
}
