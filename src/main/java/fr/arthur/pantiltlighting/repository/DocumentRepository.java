package fr.arthur.pantiltlighting.repository;

import fr.arthur.pantiltlighting.model.main.Document;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DocumentRepository extends JpaRepository<Document, Integer> {
}
