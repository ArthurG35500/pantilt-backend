package fr.arthur.pantiltlighting.repository;

import fr.arthur.pantiltlighting.model.main.Pdf;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PdfRepository extends JpaRepository<Pdf, Integer> {
    Pdf getPdfByActiveTrue();
}
