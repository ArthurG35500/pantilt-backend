package fr.arthur.pantiltlighting.repository;

import fr.arthur.pantiltlighting.model.main.Slider;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.Set;

@Repository
public interface SliderRepository extends JpaRepository<Slider, Integer> {

    @Query("SELECT g.id FROM Slider g")
    Set<Integer> findAllIds();

    Optional<Slider> findByImageId(Integer idImage);
}
