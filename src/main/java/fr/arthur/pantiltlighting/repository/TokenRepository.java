package fr.arthur.pantiltlighting.repository;


import fr.arthur.pantiltlighting.model.main.Token;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TokenRepository extends JpaRepository<Token, Integer> {
    Optional<Token> findByJti(String jti);
}
