package fr.arthur.pantiltlighting.mapper.reception;

import fr.arthur.pantiltlighting.api.dto.reception.ReceptionDocumentDto;
import fr.arthur.pantiltlighting.model.reception.ReceptionDocument;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ReceptionDocumentMapper {

    ReceptionDocumentDto mapToDto(ReceptionDocument receptionDocument);

    ReceptionDocument mapToModel(ReceptionDocumentDto receptionDocumentDto);
}

