package fr.arthur.pantiltlighting.mapper.reception;

import fr.arthur.pantiltlighting.api.dto.reception.ReceptionSliderDto;
import fr.arthur.pantiltlighting.model.reception.ReceptionSlider;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ReceptionSliderMapper {

    ReceptionSliderDto mapToDto(ReceptionSlider receptionSlider);

    ReceptionSlider mapToModel(ReceptionSliderDto receptionSliderDto);
}
