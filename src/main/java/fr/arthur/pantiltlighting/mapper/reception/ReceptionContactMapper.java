package fr.arthur.pantiltlighting.mapper.reception;

import fr.arthur.pantiltlighting.api.dto.reception.ReceptionContactDto;
import fr.arthur.pantiltlighting.model.reception.ReceptionContact;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ReceptionContactMapper {
    ReceptionContactDto mapToDto(ReceptionContact receptionContact);

    ReceptionContact mapToModel(ReceptionContactDto receptionContactDto);
}
