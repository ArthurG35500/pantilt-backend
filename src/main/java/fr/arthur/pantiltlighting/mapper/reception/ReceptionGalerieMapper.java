package fr.arthur.pantiltlighting.mapper.reception;

import fr.arthur.pantiltlighting.api.dto.reception.ReceptionGalerieDto;
import fr.arthur.pantiltlighting.model.reception.ReceptionGalerie;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ReceptionGalerieMapper {
    ReceptionGalerieDto mapToDto(ReceptionGalerie receptionGalerie);

    ReceptionGalerie mapToModel(ReceptionGalerieDto receptionGalerieDto);
}