package fr.arthur.pantiltlighting.mapper.main;

import fr.arthur.pantiltlighting.api.dto.main.ImageDto;
import fr.arthur.pantiltlighting.model.main.Image;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface ImageMapper {

    @Mapping(target = "id", source = "id")
    ImageDto mapToDto(Image image);

    @Mapping(target = "id", source = "id")
    Image mapToModel(ImageDto imageDto);

}
