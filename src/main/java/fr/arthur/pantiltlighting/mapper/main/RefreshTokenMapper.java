package fr.arthur.pantiltlighting.mapper.main;

import fr.arthur.pantiltlighting.api.dto.main.RefreshTokenDto;
import fr.arthur.pantiltlighting.model.main.RefreshToken;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface RefreshTokenMapper {
    @Mapping(target = "id", source = "id")
    RefreshTokenDto mapToDto(RefreshToken refreshToken);

    @Mapping(target = "id", source = "id")
    RefreshToken mapToModel(RefreshTokenDto refreshTokenDto);
}
