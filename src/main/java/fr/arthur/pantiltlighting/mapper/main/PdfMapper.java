package fr.arthur.pantiltlighting.mapper.main;

import fr.arthur.pantiltlighting.api.dto.main.PdfDto;
import fr.arthur.pantiltlighting.model.main.Pdf;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface PdfMapper {

    @Mapping(target = "id", source = "id")
    PdfDto mapToDto(Pdf pdf);

    @Mapping(target = "id", source = "id")
    Pdf mapToModel(PdfDto pdfDto);

}
