package fr.arthur.pantiltlighting.mapper.main;


import fr.arthur.pantiltlighting.api.dto.main.UserDto;
import fr.arthur.pantiltlighting.model.main.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.stereotype.Component;

@Component
@Mapper(componentModel = "spring")
public interface UserMapper {

    @Mapping(target = "changePassword", source = "changePassword")
    @Mapping(target = "role", source = "role")
    @Mapping(target = "password", source = "password")
    @Mapping(target = "id", source = "id")
    UserDto mapToDto(User user);

    @Mapping(target = "changePassword", source = "changePassword")
    @Mapping(target = "role", source = "role")
    @Mapping(target = "password", source = "password")
    @Mapping(target = "id", source = "id")
    User mapToModel(UserDto userDto);
}
