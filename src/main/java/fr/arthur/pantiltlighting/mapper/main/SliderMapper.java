package fr.arthur.pantiltlighting.mapper.main;

import fr.arthur.pantiltlighting.api.dto.main.SliderDto;
import fr.arthur.pantiltlighting.model.main.Slider;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface SliderMapper {

    @Mapping(target = "id", source = "id")
    SliderDto mapToDto(Slider slider);

    @Mapping(target = "id", source = "id")
    Slider mapToModel(SliderDto sliderDto);

}