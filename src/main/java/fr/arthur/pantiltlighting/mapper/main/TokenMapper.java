package fr.arthur.pantiltlighting.mapper.main;


import fr.arthur.pantiltlighting.api.dto.main.TokenDto;
import fr.arthur.pantiltlighting.model.main.Token;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface TokenMapper {
    @Mapping(target = "id", source = "id")
    TokenDto mapToDto(Token token);

    @Mapping(target = "id", source = "id")
    Token mapToModel(TokenDto tokenDto);
}
