package fr.arthur.pantiltlighting.mapper.main;

import fr.arthur.pantiltlighting.api.dto.main.DisplayPdfDto;
import fr.arthur.pantiltlighting.model.main.DisplayPdf;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface DisplayPdfMapper {

    @Mapping(target = "id", source = "id")
    DisplayPdfDto mapToDto(DisplayPdf displayPdf);

    @Mapping(target = "id", source = "id")
    DisplayPdf mapToModel(DisplayPdfDto displayPdfDto);
}
