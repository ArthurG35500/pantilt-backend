package fr.arthur.pantiltlighting.mapper.main;

import fr.arthur.pantiltlighting.api.dto.main.DocumentDto;
import fr.arthur.pantiltlighting.model.main.Document;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface DocumentMapper {

    @Mapping(target = "id", source = "id")
    DocumentDto mapToDto(Document document);

    @Mapping(target = "id", source = "id")
    Document mapToModel(DocumentDto documentDto);
}
