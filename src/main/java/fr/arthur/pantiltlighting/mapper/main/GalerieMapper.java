package fr.arthur.pantiltlighting.mapper.main;

import fr.arthur.pantiltlighting.api.dto.main.GalerieDto;
import fr.arthur.pantiltlighting.model.main.Galerie;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface GalerieMapper {

    @Mapping(target = "id", source = "id")
    GalerieDto mapToDto(Galerie galerie);

    @Mapping(target = "id", source = "id")
    Galerie mapToModel(GalerieDto galerieDto);
}
