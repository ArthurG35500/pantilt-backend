package fr.arthur.pantiltlighting.config;

import fr.arthur.pantiltlighting.model.main.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;

@AllArgsConstructor
@Data
@Getter
@Setter
public class MyUserDetails implements UserDetails {

    private final User user;
    @Setter
    private Integer id;
    private Collection<? extends GrantedAuthority> authorities;
    private String password;
    private Boolean changePassword;

    public MyUserDetails(User user) {
        this.user = user;
        this.id = user.getId();
        this.authorities = user.getAuthorities();
        this.password = user.getPassword();
        this.changePassword = user.getChangePassword();
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        SimpleGrantedAuthority auth = new SimpleGrantedAuthority(user.getRole().toString());
        return List.of(auth);
    }

    @Override
    public String getPassword() {
        return this.user.getPassword();
    }

    @Override
    public String getUsername() {
        return this.user.getUsername();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
