package fr.arthur.pantiltlighting.config;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutHandler;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CustomLogoutHandler implements LogoutHandler {
    @Override
    public void logout(HttpServletRequest request, HttpServletResponse response, Authentication authentication) {
        for (Cookie cookieName : request.getCookies()) {
            javax.servlet.http.Cookie cookie = new javax.servlet.http.Cookie(cookieName.toString(), null);
            cookie.setMaxAge(0);
            response.addCookie(cookie);
        }
    }
}
