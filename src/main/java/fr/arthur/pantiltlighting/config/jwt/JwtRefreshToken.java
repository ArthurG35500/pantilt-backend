package fr.arthur.pantiltlighting.config.jwt;


public class JwtRefreshToken {
    private String refreshToken;

    public JwtRefreshToken() {
    }

    public JwtRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }
}
