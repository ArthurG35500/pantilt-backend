package fr.arthur.pantiltlighting.config.jwt;

import fr.arthur.pantiltlighting.config.MyUserDetails;
import fr.arthur.pantiltlighting.service.impl.RefreshTokenServiceImpl;
import fr.arthur.pantiltlighting.service.impl.TokenServiceImpl;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.time.Instant;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;


@Component
public class JwtTokenUtil {
    @Value("${jwt.secretkey}")
    private String SECRET_KEY;
    @Value("${jwt.tokenexpirationtime}")
    private Integer TOKEN_EXPIRATION_TIME;
    @Value("${jwt.refreshtokenexpirationtime}")
    private Integer REFRESHTOKEN_EXPIRATION_TIME;

    @Autowired
    private TokenServiceImpl tokenService;
    @Autowired
    private RefreshTokenServiceImpl refreshTokenService;


    public String generateToken(MyUserDetails userDetails) {
        return createToken(userDetails, TOKEN_EXPIRATION_TIME);
    }

    public String generateRefreshToken(MyUserDetails userDetails) {
        return createToken(userDetails, REFRESHTOKEN_EXPIRATION_TIME);
    }

    private String createToken(MyUserDetails userDetails, Integer tokenExpirationTime) {
        Map<String, Object> claims = new HashMap<>();
        claims.put("roles", userDetails.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.toSet()));
        claims.put("id", userDetails.getId());
        String uuid = generateUniqueUuid();
        claims.put("jti", uuid);

        return Jwts.builder()
                .setClaims(claims)
                .setSubject(userDetails.getUsername())
                .setIssuedAt(Date.from(Instant.now()))
                .setExpiration(Date.from(Instant.now().plus(Duration.ofMillis(tokenExpirationTime))))
                .signWith(SignatureAlgorithm.HS512, SECRET_KEY)
                .compact();
    }

    public Boolean validateToken(String token, MyUserDetails userDetails) {
        final Integer user = getUserIdFromToken(token);
        return (user.equals(userDetails.getId()) && !isTokenExpired(token));
    }

    private Boolean isTokenExpired(String token) {
        final Date expiration = getExpirationDateFromToken(token);
        return expiration.before(new Date());
    }

    public Date getExpirationDateFromToken(String token) {
        return Jwts.parser().setSigningKey(SECRET_KEY).parseClaimsJws(token).getBody().getExpiration();
    }

    public Integer getUserIdFromToken(String token) {
        return Jwts.parser().setSigningKey(SECRET_KEY).parseClaimsJws(token).getBody().get("id", Integer.class);
    }

    public Date getCreationDateFromToken(String token) {
        return Jwts.parser().setSigningKey(SECRET_KEY).parseClaimsJws(token).getBody().getIssuedAt();
    }

    public String getJtiFromToken(String token) {
        return Jwts.parser().setSigningKey(SECRET_KEY).parseClaimsJws(token).getBody().get("jti", String.class);
    }

    private String generateUniqueUuid() {
        String uuid;
        do {
            uuid = UUID.randomUUID().toString();
        } while (tokenService.doesJtiExist(uuid) || refreshTokenService.doesJtiExist(uuid));
        return uuid;
    }

}
