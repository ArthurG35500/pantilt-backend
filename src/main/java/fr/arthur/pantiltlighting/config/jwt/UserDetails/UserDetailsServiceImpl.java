package fr.arthur.pantiltlighting.config.jwt.UserDetails;


import fr.arthur.pantiltlighting.config.MyUserDetails;
import fr.arthur.pantiltlighting.model.main.User;
import fr.arthur.pantiltlighting.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    @Autowired
    private UserRepository userRepository;

    @Override
    public MyUserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> optionalUser = userRepository.findByUsername(username);
        User user = optionalUser.orElseThrow(() -> new UsernameNotFoundException("User not found with username: " + username));
        return new MyUserDetails(user);
    }

    public MyUserDetails loadUserById(Integer id) throws UsernameNotFoundException {
        Optional<User> optionalUser = userRepository.findById(id);
        User user = optionalUser.orElseThrow(() -> new UsernameNotFoundException("User not found with ID; " + id));
        return new MyUserDetails(user);
    }

}


