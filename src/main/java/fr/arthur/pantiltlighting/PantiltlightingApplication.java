package fr.arthur.pantiltlighting;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PantiltlightingApplication {

    public static void main(String[] args) {
        SpringApplication.run(PantiltlightingApplication.class, args);
    }

}
