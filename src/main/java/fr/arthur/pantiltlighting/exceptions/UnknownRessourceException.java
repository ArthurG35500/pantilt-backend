package fr.arthur.pantiltlighting.exceptions;

public class UnknownRessourceException extends RuntimeException {

    public UnknownRessourceException(String message) {
        super(message);
    }
}
