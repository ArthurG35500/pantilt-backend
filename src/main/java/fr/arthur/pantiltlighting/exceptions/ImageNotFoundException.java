package fr.arthur.pantiltlighting.exceptions;

/**
 * Exception thrown when an image is not found.
 */
public class ImageNotFoundException extends RuntimeException {

    /**
     * Constructs a new ImageNotFoundException with the specified detail message.
     *
     * @param message the detail message.
     */
    public ImageNotFoundException(String message) {
        super(message);
    }

}

